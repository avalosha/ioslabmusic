//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by Álvaro Ávalos Hernández on 11/9/18.
//  Copyright © 2018 Álvaro Ávalos Hernández. All rights reserved.
//

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {

    override func setUp() {
        //En el setup se debe utilizar para el limpiado de la BD al hacer testing
        let session = Session.sharedInstance
        session.token = nil
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
/*
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    */
    func testCorrectLogin(){
        XCTAssertTrue(User.login(yourname: "iosLab", password: ""))
    }
    
    func testWrongLogin(){
        XCTAssertFalse(User.login(yourname: "carlos", password: ""))
    }

    func testSaveSession(){
        let session = Session.sharedInstance //accediendo al singleton
        let _ = User.login(yourname: "iosLab", password: "223")
        XCTAssertNotNil(session.token)
    }
    
    func testWrongSaveSession(){
        let session = Session.sharedInstance //accediendo al singleton
        let _ = User.login(yourname: "Alvaro", password: "223")
        XCTAssertNil(session.token)
    }
    
    func testExpectedToken(){
        let _ = User.login(yourname: "iosLab", password: "")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token!, "123456789", "Token Should Match")
    }
    
    func testWrongExpectedToken(){
        let _ = User.login(yourname: "iosLab", password: "")
        let session = Session.sharedInstance
        XCTAssertNotEqual(session.token!, "1234567890", "Not equal")
    }
    
    func testThrowsError(){
        XCTAssertThrowsError(try User.fetchSongs())
    }
    
    func testMusicSongs(){
        var resultSongs: [Song] = []
        let promise = expectation(description: "Songs Downloaded")
        Music.fetchSongs{(songs) in
            resultSongs = songs
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSongs.count, 0)
    }
}

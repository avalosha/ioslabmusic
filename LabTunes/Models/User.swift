//
//  User.swift
//  LabTunes
//
//  Created by Álvaro Ávalos Hernández on 11/9/18.
//  Copyright © 2018 Álvaro Ávalos Hernández. All rights reserved.
//

import Foundation

class User {
    static let userName = "iosLab"
    static let password = "verysecurepassword"
    static let session = Session.sharedInstance
    
    static func login(yourname: String, password: String) -> Bool {
        if self.userName == yourname {
            session.saveSession()
            return true
        }
        return false
    }
    
    static func fetchSongs() throws {
        guard let token = Session.sharedInstance.token else {
            throw UserError.notSessionAvailable
        }
        debugPrint(token)
    }
    
    enum UserError: Error {
        case notSessionAvailable
    }
}

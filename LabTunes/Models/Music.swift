//
//  Music.swift
//  LabTunes
//
//  Created by Álvaro Ávalos Hernández on 11/9/18.
//  Copyright © 2018 Álvaro Ávalos Hernández. All rights reserved.
//

import Foundation

class Music {
    static var urlSession = URLSession(configuration: .default)
    
    //con escaping se ejecuta el clousure hasta que responda el servicio de iTunes
    static func fetchSongs(songName: String = "TheBeatles", onSuccess: @escaping ([Song]) -> Void){
        //parsear los espacios para ocuparlo en el URL*****************************************************
        let url = URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=\(songName)")
        //lo ejecuta en otro hilo
        let dataTask = urlSession.dataTask(with: url!) {data, response, error in
            if error == nil {
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {return}
                if statusCode == 200 {
                    guard let json = parseData(data: data!) else {return}
                    let songs = songsFrom(json: json)
                    onSuccess(songs)
                }
            }
        }
        dataTask.resume()
    }
    
    static func parseData(data: Data) -> NSDictionary? {
        let json = try! JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
        return json
    }
    
    static func songsFrom(json: NSDictionary) -> [Song] {
        let results = json["results"] as! [NSDictionary]
        var songs: [Song] = []
        for dataResult in results {
            let song = Song.create(dict: dataResult)
            songs.append(song!)
        }
        return songs
    }
}

//
//  Session.swift
//  LabTunes
//
//  Created by Álvaro Ávalos Hernández on 11/9/18.
//  Copyright © 2018 Álvaro Ávalos Hernández. All rights reserved.
//

import Foundation

class Session: NSObject {
    var token: String?
    static let sharedInstance = Session()
    
    override private init() {
        super.init()
    }
    
    func saveSession(){
        token = "123456789"
    }
}
